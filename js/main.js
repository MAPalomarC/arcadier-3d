/// <reference path="./vendor/babylon.d.ts"/>

const canvas = document.getElementById('renderCanvas');

const motor = new BABYLON.Engine(canvas,true)

function crearEscena(){
    const escena = new BABYLON.Scene(motor);

    const camara = new BABYLON.FreeCamera("camara", new BABYLON.Vector3(0,1,-5), escena)
    camara.attachControl(canvas, true);
    const luz = new BABYLON.HemisphericLight("luz", new BABYLON.Vector3(0,2,0),escena)

    BABYLON.SceneLoader.ImportMeshAsync("","../assets/","ae86.glb",escena)

    return escena
}

const escena = new crearEscena();

motor.runRenderLoop(() => {
    escena.render();
});